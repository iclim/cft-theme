jQuery(document).ready(function($) {


	// have to add a disabled class here to get navigation dropdowns to act like... NAVIGATION DROPDOWNS
	// this theme is utter shit
	// a corresponding CSS rule exists to display the dropdowns on hover... 
	// because it doesnt do that by default either
	$('nav ul.menu .dropdown').find('a.dropdown-toggle').each(function(){
		$(this).addClass('disabled');
	})



	// PROJECT MAP

	if ($('#projects-map').length > 0 ){


		var demoData = [
			{
				title: "Fiji",
				coords: [178.0650, -17.7134],
				funds: [
					{
						name: "Green Climate Fund",
						projects: [
							{
								name: "Fiji Urban Water Supply and Wastewater Management Project",
								link: "http://www.greenclimate.fund/-/fiji-urban-water-supply-and-wastewater-management-project?inheritRedirect=true&redirect=%2Fventures%2Fportfolio"
							} 
						]
					},
					{
						name:"GEF Special Climate Change Fund",
						projects: [
							{
								name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
								link: "https://www.sprep.org/pacc"
							}
						]
					}
				]
			},
			{
				title: "Vanuatu",
				coords: [166.9592, -15.3767],
				funds: [
					{
						name: "Green Climate Fund",
						projects: [
							{
								name: "Readiness Activity: NDA strengthening and country programme",
								link: "http://www.greenclimate.fund/documents/20182/93876/2.15.17_-_Vanuatu_Readiness_Proposal.pdf/1a5ce6c2-e199-4088-a102-6596cba88932"
							},
							{
								name: "Readiness Activity: pipeline development"
							}  
						]
					},
					{
						name:"GEF Special Climate Change Fund",
						projects: [
							{
								name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
								link: "https://www.sprep.org/pacc"
							}
						]
					}
				]
			}
		];

		var demoData = [
		{
		    title: "Fiji",
		    coords: [178.0650, -17.7134],
		    funds: [
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Fiji Urban Water Supply and Wastewater Management Project",
			            link: "http://www.greenclimate.fund/-/fiji-urban-water-supply-and-wastewater-management-project?inheritRedirect=true&redirect=%2Fventures%2Fportfolio"
			        },
			        {
			            name: "Readiness: Country programme (pipeline)",
			            link: ""
			        }
		        ]
		    }, 
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [{
		            name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            link: "https://www.sprep.org/pacc"
		        }]
		    }]
		}, {
		    title: "Vanuatu",
		    coords: [166.9592, -15.3767],
		    funds: [
		    	{
		                name: "Green Climate Fund",
		                projects: 
		                		[
		                		{
		                            name: "Climate Information Services for resilient development in Vanuatu ",
		                            link: "http://www.greenclimate.fund/-/climate-information-services-for-resilient-development-in-vanuatu"
		                        }, 
		                        {
		                            name: "Readiness Activity: NDA strengthening and country programme",
		                            link: "http://www.greenclimate.fund/documents/20182/93876/2.15.17_-_Vanuatu_Readiness_Proposal.pdf/1a5ce6c2-e199-4088-a102-6596cba88932"
		                        },
		                        {
		                            name: "Readiness Activity: Strategic Frameworks (pipeline)",
		                            link: ""
		                        },
		                        {
		                            name: "Readiness Activity: Adaptation Planning - NAP (pipeline)",
		                            link: ""
		                        }
					]
				},
				{
					name:"GEF Special Climate Change Fund",
					projects: 
						[
							{
								name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
								link: "https://www.sprep.org/pacc"
							},
						]
				},
				{
					name:"GEF Least Developed Countries Fund",
					projects: 
						[
							{
								name: "Vanuatu Coastal Adaptation Project",
								link: ""
							},
							{
								name: "Increasing Resilience to Climate Change and Natural Hazards Project",
								link: ""
							},
							{
								name: "Vanuatu National Adaptation Programme of Action (NAPA)",
								link: ""
							}
						]
				}
			]
		},
		{
		    title: "Solomon Islands",
		    coords: [159.95, -9.43333],
		    funds: [
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Tina River Hydropower Development Project",
			            link: ""
			        },
			        {
			            name: "Readiness Activity: NDA strengthening and country programme (pipeline)",
			            link: ""
			        }
		        ]
		    }, 
		    {
		        name: "GEF Least Developed Countries Fund",
		        projects: [
		        	{
		            	name: "Community Resilience to Climate and Disaster Risk in Solomon Islands Project",
		            	link: "http://documents.worldbank.org/curated/en/620371468102860121/Community-Resilience-to-Climate-and-Disaster-Risk-in-Solomon-Islands-Project-CRISP"
		        	},
		        	{
		            	name: "Formulation of a National Adaptation Programme of Action (NAPA) for Solomon Islands",
		            	link: "http://adaptation-undp.org/projects/solomon-islands-national-adaptation-programme-action-napa"
		        	},
		        	{
		            	name: "Solomon Islands Water Sector Adaptation Project (SIWSAP)",
		            	link: "http://adaptation-undp.org/projects/ldcf-siwsap"
		        	},
		        ]
		    },
		    {
		        name: "Adaptation Fund",
		        projects: [
		        	{
		            	name: "Enhancing resilience of communities in Solomon Islands to the adverse effects of climate change in agriculture and food security",
		            	link: "https://www.adaptation-fund.org/project/enhancing-resilience-of-communities-in-solomon-islands-to-the-adverse-effects-of-climate-change-in-agriculture-and-food-security/"
		        	}
		        ]
		    }
		    ]
		},
		{
		    title: "Papua New Guinea",
		    coords: [143.95555, -6.31499],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Adaptation Fund",
		        projects: [
		        	{
		            	name: "Enhancing adaptive capacity of communities to climate change-related floods in the North Coast and Islands Region of Papua New Guinea",
		            	link: "https://www.adaptation-fund.org/project/enhancing-adaptive-capacity-of-communities-to-climate-change-related-floods-in-the-north-coast-and-islands-region-of-papua-new-guinea/"
		        	}
		        ]
		    },
		    {
		        name: "Pilot Program for Climate Resilience",
		        projects: [
		        	{
		            	name: "Building Resilience to Climate Change in Papua New Guinea",
		            	link: "https://www.adb.org/projects/46495-002/main"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Global Energy Efficiency and Renewable Energy Fund (GEEREF NeXt) - PNG",
			            link: "",
			        },
			        {
			            name: "Readiness Activity: Adaptation Planning - NAP (pipeline)",
			            link: "",
			        }		        ]
		    }
		    ]
		},
		{
		    title: "Palau",
		    coords: [134.58252, 7.51498],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Readiness Activity: NDA strengthening and country programme",
			            link: "",
			        }		        ]
		    }
		    ]
		},
		{
		    title: "Tonga",
		    coords: [-175.19824, -21.17899],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Readiness Activity: NDA strengthening and country programme",
			            link: ""
			        }		        
			     ]
		    }
		    ]
		},
		{
		    title: "Cook Islands",
		    coords: [-159.77767, -21.23674],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		     {
		        name: "Adaptation Fund",
		        projects: [
		        	{
		            	name: "Strengthening the Resilience of our Islands and our Communities to Climate Change",
		            	link: "https://www.adaptation-fund.org/project/strengthening-the-resilience-of-our-islands-and-our-communities-to-climate-change/"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Pacific Islands Renewable Energy Investment Program",
			            link: "http://www.greenclimate.fund/-/pacific-islands-renewable-energy-investment-program?"
			        },
			        {
			            name: "Readiness Activity: NDA strengthening",
			            link: "http://www.greenclimate.fund/documents/20182/93876/2.15.2_-_Cook_Island_Readiness_Proposal.pdf/177c612d-2252-4256-9fca-de8e968b17a1"
			        },
			        {
			            name: "Readiness Activity: Entity support (pipeline)",
			            link: ""
			        }

			     ]
		    }
		    ]
		},
		{
		    title: "Samoa",
		    coords: [-172.10463, -13.75903],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "GEF Least Developed Countries Fund",
		        projects: [
		        	{
		            	name: "Integration of Climate Change Risk and Resilience into Forestry Management",
		            	link: ""
		        	},
		        	{
		            	name: "Enhancing the Resilience of Tourism reliant communities to climate change risks",
		            	link: ""
		        	},
		        	{
		            	name: "Integrating Climate Change Risks into the Agriculture and Health Sectors in Samoa",
		            	link: ""
		        	},
		        	{
		            	name: "Economy‐wide integration to climate change adaptation and disaster risk management and reduction to reduce climate vulnerability of communities in Samoa",
		            	link: ""
		        	},
		        	{
		            	name: "Samoa National Adaptation Programme of Action (NAPA)",
		            	link: ""
		        	}
		        ]
		    },
		     {
		        name: "Adaptation Fund",
		        projects: [
		        	{
		            	name: "Enhancing Resilience of Samoa's Coastal Communities to Climate Change",
		            	link: "http://www.adaptation-undp.org/projects/af-samoa"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Integrated flood management to enhance climate resilience of the Vaisigano River Catchment in Samoa",
			            link: "http://www.greenclimate.fund/-/integrated-flood-management-to-enhance-climate-resilience-of-the-vaisigano-river-catchment-in-samoa"
			        },
			        {
			            name: "Readiness Activity: Climate-resilient agricultural livelihoods (pipeline)",
			            link: ""
			        },
			        {
			            name: "Readiness Activity: Entity support (pipeline)",
			            link: ""
			        }

			     ]
		    }
		    ]
		},
		{
		    title: "Niue",
		    coords: [-169.86723, -19.05444],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Readiness Activity: Entity support",
			            link: "",
			        }		        ]
		    }
		    ]
		},
		{
		    title: "Nauru",
		    coords: [166.93150, -0.52278],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Readiness Activity: NDA strengthening and country programme",
			            link: "",
			        },
			        {
			            name: "Readiness Activity: Adaptation Planning - NAP (pipeline)",
			            link: "",
			        }		     		        ]
		    }
		    ]
		},
		{
		    title: "Kiribati",
		    coords: [-157.36286, 1.87094],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Readiness Activity: NDA strengthening and country programme",
			            link: "",
			        }	     		        ]
		    },
		    {
		        name: "GEF Least Developed Countries Fund",
		        projects: [
			        {
			            name: "Enhancing National Food Security in the Context of Climate Change",
			            link: "",
			        },
			        {
			            name: "Increasing Resilience to Climate Variability and Hazards (KAP III)",
			            link: "",
			        },
			        {
			            name: "Kiribati National Adaptation Programme of Action (NAPA)",
			            link: "",
			        }	     		        
			    ]
		    }
		    ]
		},
		{
		    title: "Tuvalu",
		    coords: [178.67992, -7.47842],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Tuvalu Coastal Adaptation Project",
			            link: "http://www.greenclimate.fund/-/tuvalu-coastal-adaptation-project",
			        }	     		        ]
		    },
		    {
		        name: "GEF Least Developed Countries Fund",
		        projects: [
			        {
			            name: "Effective and Responsive island-level governance to secure and diversify climate resilient marine-based coastal livelihoods and enhance climate hazard response capacity",
			            link: "",
			        },
			        {
			            name: "Increasing Resilience of Coastal Areas and Community Settlements to Climate Change in Tuvalu",
			            link: "",
			        },
			        {
			            name: "Tuvalu National Adaptation Programme of Action (NAPA)",
			            link: "",
			        }	     		        
			    ]
		    }
		    ]
		},
		{
		    title: "Federated States of Micronesia",
		    coords: [158.22252, 6.88746],
		    funds: [
		    
		    {
		        name: "GEF Special Climate Change Fund",
		        projects: [
		        	{
		            	name: "Pacific Adaptation to Climate Change (PACC) Programme 2009 -2014",
		            	link: "https://www.sprep.org/pacc"
		        	}
		        ]
		    },
		    {
		        name: "Green Climate Fund",
		        projects: [
			        {
			            name: "Readiness Activity: NDA strengthening and country programme",
			            link: "http://www.greenclimate.fund/documents/20182/93876/2.15.7_-_FSM_Readiness_Proposal.pdf/497833dc-db8e-4e4f-a38c-281621a226c6",
			        },
			        {
			            name: "Readiness Activity: Entity support",
			            link: "",
			        }	     		        ]
		    }
		    ]
		}
];

        


	    /**
         * Elements that make up the popup.
         */
        var popupContainer = document.getElementById('popup');
        var popupContent = document.getElementById('popup-content');
        var popupCloser = document.getElementById('popup-closer');


        /**
         * Create an overlay to anchor the popup to the map.
         */
        var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
          element: popupContainer,
          autoPan: true,
          autoPanAnimation: {
            duration: 250
          }
        }));


        


        var raster = new ol.layer.Tile({
          source: new ol.source.OSM()
        });

        var markerSource = new ol.source.Vector({
	        features: [],
	        wrapX: false
	    });

	    var markerLayer = new ol.layer.Vector({
	        source: markerSource
	    });

	    
        /**
         * Create the map.
         */
        var map = new ol.Map({
          layers: [raster, markerLayer],
          overlays: [overlay],
          target: 'projects-map',
          view: new ol.View({
            center: [0, 0],
            zoom: 2
          })
        });


        /**
         * Add a click handler to the map to render the popup.
         */
       //map.on('singleclick', function(evt) {
       //  var coordinate = evt.coordinate;
       //  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
       //      coordinate, 'EPSG:3857', 'EPSG:4326'));

       //  content.innerHTML = '<p>You clicked here:</p><code>' + hdms +
       //      '</code>';
       //  overlay.setPosition(coordinate);
       //});
        var featureSource = new ol.source.Vector({
	        features: [],
	        wrapX: false
	    });

        var featureOverlay = new ol.layer.Vector({
            source: featureSource,
            name: 'Features',
            type: 'hover-overlay',
            style: new ol.style.Style({
		        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
		          anchor: [0.5, 46],
		          anchorXUnits: 'fraction',
		          anchorYUnits: 'pixels',
		          src: 'themes/DST/images/marker-hover.png'
		        }))
		    }),
            visible: true
        });

		map.addLayer(featureOverlay);

        $.each(demoData, function(i, point){
        	
        	if (point.coords[0] < -140) {
				point.coords[0] = point.coords[0]+360; 
				// console.log('adding 360');
			}
        	var location = ol.proj.transform(point.coords, 'EPSG:4326','EPSG:3857');

        	// console.log(location);

        	var iconFeature = new ol.Feature({
		        geometry: new ol.geom.Point(location),
		        name: point.title,
		        funds: point.funds
		    });

		    var iconStyle = new ol.style.Style({
		        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
		          anchor: [0.5, 46],
		          anchorXUnits: 'fraction',
		          anchorYUnits: 'pixels',
		          src: 'themes/DST/images/marker.png'
		        })),
		        text: new ol.style.Text({
			        font: '12px Calibri,sans-serif',
			        fill: new ol.style.Fill({ color: '#000' }),
			        stroke: new ol.style.Stroke({
			          color: '#fff', width: 2
			        }),
			        // get the text from the feature - `this` is ol.Feature
			        text: point.title
			      })
		    });

		    iconFeature.setStyle(iconStyle);

		    markerSource.addFeature(iconFeature);
        });


        // fit all markers on map
        var extent = markerSource.getExtent();
        map.getView().fit(extent, {padding: [100,100,100,100]});


        var select_interaction = new ol.interaction.Select();
        select_interaction.setHitTolerance(25);

		select_interaction.getFeatures().on("add", function (e) { 

		    var feature = e.element; //the feature selected

		    var html = '<h3>'+feature.get('name')+'</h3>';

		    $.each(feature.get('funds'), function(i, fund){
		    	html += '<h4>'+fund.name+'</h4>';
		    	html += '<ul>';
		    	$.each(fund.projects, function(i, project){
		    		if (project.link.length){
		    			html += '<li><a href="'+project.link+'" target="_blank">'+project.name+'</a></li>';	
		    		} else {
		    			html += '<li>'+project.name+'</li>';	
		    		}
		    		
		    	});
		    	html += '</ul>';
		    })

		    popupContent.innerHTML = html;

          	overlay.setPosition(feature.getGeometry().getCoordinates());
		});

		map.addInteraction(select_interaction);

		/**
         * Add a click handler to hide the popup.
         * @return {boolean} Don't follow the href.
         */
        popupCloser.onclick = function() {
          overlay.setPosition(undefined);
          popupCloser.blur();
          select_interaction.getFeatures().clear();;
          return false;
        };
        
        map.on('pointermove', function(evt) {
	        if (evt.dragging) return;
            
            featureOverlay.getSource().clear();
            
            var pixel = map.getEventPixel(evt.originalEvent);
            var hit = map.hasFeatureAtPixel(pixel);
            
            map.getTargetElement().style.cursor = hit ? 'pointer' : '';

            var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
	          return feature;
	        });
            
            if(hit){
                featureOverlay.getSource().addFeature(feature);
            } else {
                featureOverlay.getSource().clear();
            }
            
            return;
	    });


    }

});