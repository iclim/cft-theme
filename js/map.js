//map vars
var map = false;
var markerLayer = false;
var markerSource = false;
var raster = false;
var overlay = false;
var featureSource = false;
var featureOverlay = false;
var popupContainer = false;
var popupContent = false;
var popupCloser = false;
var select_interaction = false;


function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
}

function arrayUnique(array) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}

function isSelected(value) {
    return value.selected;
}



var countries = [{
    title: "Fiji",
    coords: [178.0650, -17.7134],
    selected: true,
}, {
    title: "Vanuatu",
    coords: [166.9592, -15.3767],
    selected: true

}, {
    title: "Solomon Islands",
    coords: [159.95, -9.43333],
    selected: true
}, {
    title: "Papua New Guinea",
    coords: [143.95555, -6.31499],
    selected: true
}, {
    title: "Palau",
    coords: [134.58252, 7.51498],
    selected: true
}, {
    title: "Tonga",
    coords: [-175.19824, -21.17899],
    selected: true
}, {
    title: "Cook Islands",
    coords: [-159.77767, -21.23674],
    selected: true
}, {
    title: "Samoa",
    coords: [-172.10463, -13.75903],
    selected: true
}, {
    title: "Niue",
    coords: [-169.86723, -19.05444],
    selected: true
}, {
    title: "Nauru",
    coords: [166.93150, -0.52278],
    selected: true
}, {
    title: "Kiribati",
    coords: [-157.36286, 1.87094],
    selected: true
}, {
    title: "Tuvalu",
    coords: [178.67992, -7.47842],
    selected: true
}, {
    title: "Federated States of Micronesia",
    coords: [158.22252, 6.88746],
    selected: true
}];



jQuery(document).ready(function($) {
    popupContainer = document.getElementById('popup');
    popupContent = document.getElementById('popup-content');
    popupCloser = document.getElementById('popup-closer');

    $('nav ul.menu .dropdown').find('a.dropdown-toggle').each(function() {
        $(this).addClass('disabled');
    })

    // PROJECT MAP

    if ($('#region_app').length > 0) {

        //Vue script here
        var region_app = new Vue({
            el: '#region_app',
            delimiters: ['${', '}'],
            data: {
                message: 'Who is doing what in the region',
                show: false,
                projects: [],
                project_data: [],
                country_data: {},
                countries: countries,
                statuses: [],
                funds: [],
                selected_projects: [],
                selected_countries: [],
                country_projects: {},
                fund_projects: {},
                status_projects: {},
                project_countries: {},
            },
            mounted: function() {
                jQuery('#loading').hide();
                this.prepareCountryData();
                // this.getData();
                // this.getSelectedItems();
                this.initMap();
            },
            filters: {
                classname: function(value) {
                    if (!value) return ''
                    value = value.toString()
                    return value.replace(/\s+/g, '-').toLowerCase()
                },
                truncate: function(string) {
                    // console.log('truncate: '+string.length);
                    var len = 100;
                    if (string.length > len) {
                        return string.substring(0, len) + '...';
                    }
                    return string;
                }
            },
            computed: {
                projectCount: function() {
                    return this.getSelectedItems().selected_projects.length;
                },

            },
            methods: {
                getData: function() {
                    // console.log('Load resources')
                    var self = this;
                    // axios.get('https://pacificclimatechange.net/document-search-json?f[0]=field_tags:3142', {
                    axios.get('/themes/DST/js/projects.json', {
                            //
                        })
                        .then(function(response) {
                            // console.log(response.data);
                            self.projects = response.data;
                            response.data.forEach(function(proj, index) {
                                // console.log(proj);
                                // var status_slug = slugify(proj.status)
                                self.project_data[proj.slug] = proj;





                                if (!proj.status) {
                                    proj.status = 'Unknown';
                                }
                                var status_obj = { 'title': proj.status, 'selected': true, 'slug': slugify(proj.status) };
                                self.addStatus(status_obj);
                                if (!self.status_projects[status_obj.slug]) {
                                    self.status_projects[status_obj.slug] = [];
                                }
                                self.status_projects[status_obj.slug].push(proj.slug);


                                if (!proj.fund) {
                                    proj.fund = 'Unknown';
                                }
                                var fund_obj = { 'title': proj.fund, 'selected': true, 'slug': slugify(proj.fund) };
                                self.addFund(fund_obj);

                                if (!self.fund_projects[fund_obj.slug]) {
                                    self.fund_projects[fund_obj.slug] = [];
                                }
                                self.fund_projects[fund_obj.slug].push(proj.slug);


                                if (!proj.countries) {
                                    proj.countries = [];
                                } else {
                                    var country_array = proj.countries.split(',');
                                    // console.log(country_array);
                                    country_array.forEach(function(country, index) {
                                        var country_slug = slugify(country);
                                        if (!self.country_projects[country_slug]) {
                                            self.country_projects[country_slug] = [];
                                        }
                                        self.country_projects[country_slug].push(proj.slug);


                                        if (!self.project_countries[proj.slug]) {
                                            self.project_countries[proj.slug] = [];
                                        }
                                        self.project_countries[proj.slug].push(country_slug);
                                    });
                                }
                                // var country_obj = {'title':proj.fund, 'selected':true, 'slug':slugify(proj.fund)};
                                // self.addProjectForCountry();




                            });


                            //now we have the data so show the ui
                            self.show = true;

                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                },
                prepareCountryData: function() {
                    var self = this;
                    this.countries.forEach(function(value, index) {
                        value.slug = slugify(value.title);
                        self.country_data[value.slug] = value;
                    });
                    this.getData();


                },
                addStatus: function(status) {
                    var self = this;

                    var found = false;
                    this.statuses.forEach(function(st, index) {
                        if (status.slug == st.slug) {
                            found = true;
                        }
                    });
                    if (!found) {
                        self.statuses.push(status);
                    }
                },
                addFund: function(fund) {
                    var self = this;

                    var found = false;
                    this.funds.forEach(function(f, index) {
                        if (fund.slug == f.slug) {
                            found = true;
                        }
                    });
                    if (!found) {
                        self.funds.push(fund);
                    }
                },
                getLabelClass: function(project) {
                    if (project.status == 'ACTIVE'){
                        return 'label-info';
                    }
                    return 'label-default';
                },
                getSelectedItems: function() {
                    console.log('getSelectedItems');
                    var self = this;



                    var countries_filter = this.countries.filter(isSelected);
                    var funds_filter = this.funds.filter(isSelected);
                    var status_filter = this.statuses.filter(isSelected);

                    var selected_projects = [];
                    var selected_countries = [];

                    this.projects.forEach(function(proj, index) {
                        //check if project is matching all of the selected filters
                        countries_filter.forEach(function(country, index) {
                            if (proj.countries.indexOf(country.title) >= 0) {
                                // console.log('Found country:'+country.title);
                                funds_filter.forEach(function(fund, index) {
                                    if (proj.fund.indexOf(fund.title) >= 0) {
                                        // console.log('Found fund:'+fund.title);
                                        status_filter.forEach(function(status, index) {
                                            if (proj.status.indexOf(status.title) >= 0) {
                                                // console.log('Found fund:'+fund.title);
                                                if (selected_projects.indexOf(proj.slug) < 0) {
                                                    selected_projects.push(proj.slug);
                                                }

                                            }

                                        });
                                    }
                                });
                            }
                        });
                    });



                    //get countries for selected projects
                    selected_projects.forEach(function(value, index) {

                        if (self.project_countries[value]) {
                            // console.log(self.project_countries[value]);
                            selected_countries = arrayUnique(selected_countries.concat(self.project_countries[value]));

                        }

                    });

                    // console.log(selected_countries);       

                    var showing_countries = [];
                    selected_countries.forEach(function(value, index) {

                        //the country has to be selected to show
                        countries_filter.forEach(function(filtered_country, index) {
                            if (value == filtered_country.slug) {
                                showing_countries.push(value);
                            }
                        });

                        // if(countries_filter.indexOf(value) >= 0){

                        // }                    

                    });

                    self.selected_countries = showing_countries;
                    self.selected_projects = selected_projects;
                    var result = {
                        'selected_countries': showing_countries,
                        'selected_projects': selected_projects
                    };
                    this.addMarkersToMap();
                    return result;

                },
                initMap() {
                    popupContainer = document.getElementById('popup');
                    popupContent = document.getElementById('popup-content');
                    popupCloser = document.getElementById('popup-closer');

                    if (ol !== 'undefined') {
                        overlay = new ol.Overlay( /** @type {olx.OverlayOptions} */ ({
                            element: popupContainer,
                            autoPan: true,
                            autoPanAnimation: {
                                duration: 250
                            }
                        }));

                        // raster = new ol.layer.Tile({
                        //     source: new ol.source.OSM()
                        // });

                        raster = new ol.layer.Tile({
                            source: new ol.source.XYZ({
                                url: 'https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXJ2ZXNvbGxhbmQiLCJhIjoiY2lyeTk2YnpxMDBlOTJ5bzRlZnRvdG9zciJ9.E4_LKLYrKhSPNm63lSQ1dg'
                            })
                        });

                        markerSource = new ol.source.Vector({
                            features: [],
                            wrapX: false
                        });

                        markerLayer = new ol.layer.Vector({
                            source: markerSource
                        });

                        if (!map) {
                            map = new ol.Map({
                                layers: [raster, markerLayer],
                                overlays: [overlay],
                                target: 'projects-map',
                                view: new ol.View({
                                    center: [0, 0],
                                    zoom: 2
                                })
                            });
                        }




                        featureSource = new ol.source.Vector({
                            features: [],
                            wrapX: false
                        });

                        featureOverlay = new ol.layer.Vector({
                            source: featureSource,
                            name: 'Features',
                            type: 'hover-overlay',
                            style: new ol.style.Style({
                                image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
                                    anchor: [0.5, 46],
                                    anchorXUnits: 'fraction',
                                    anchorYUnits: 'pixels',
                                    src: '/themes/DST/images/marker-hover.png'
                                }))
                            }),
                            visible: true
                        });

                        map.addLayer(featureOverlay);

                        

                        popupCloser.onclick = function() {
                          overlay.setPosition(undefined);
                          popupCloser.blur();
                          select_interaction.getFeatures().clear();
                          return false;
                        };
                         

                        map.on('pointermove', function(evt) {
                            if (evt.dragging) return;

                            featureOverlay.getSource().clear();

                            var pixel = map.getEventPixel(evt.originalEvent);
                            var hit = map.hasFeatureAtPixel(pixel);

                            map.getTargetElement().style.cursor = hit ? 'pointer' : '';

                            var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
                                return feature;
                            });

                            if (hit) {
                                featureOverlay.getSource().addFeature(feature);
                            } else {
                                featureOverlay.getSource().clear();
                            }

                            return;
                        });
                    }

                },
                addMarkersToMap() {
                    var self = this;
                    if (typeof markerSource.clear === "function") {
                        markerSource.clear();
                        self.selected_countries.forEach(function(country, i) {
                            var point = self.country_data[country];

                            if (point.coords[0] < -140) {
                                point.coords[0] = point.coords[0] + 360;
                                // console.log('adding 360');
                            }
                            var location = ol.proj.transform(point.coords, 'EPSG:4326', 'EPSG:3857');

                            // console.log(location);

                            var iconFeature = new ol.Feature({
                                geometry: new ol.geom.Point(location),
                                name: point.title,
                                country: point,
                                projects: self.country_projects[point.slug]
                            });

                            var iconStyle = new ol.style.Style({
                                image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
                                    anchor: [0.5, 46],
                                    anchorXUnits: 'fraction',
                                    anchorYUnits: 'pixels',
                                    src: '/themes/DST/images/marker.png'
                                })),
                                text: new ol.style.Text({
                                    font: '12px Calibri,sans-serif',
                                    fill: new ol.style.Fill({ color: '#000' }),
                                    stroke: new ol.style.Stroke({
                                        color: '#fff',
                                        width: 2
                                    }),
                                    // get the text from the feature - `this` is ol.Feature
                                    text: point.title
                                })
                            });

                            iconFeature.setStyle(iconStyle);

                            markerSource.addFeature(iconFeature);
                        });
                    }

                    if (typeof markerSource.getExtent === "function" && self.selected_countries.length) {
                        // console.log(markerSource.getExtent());
                        // // fit all markers on map
                        popupContent = document.getElementById('popup-content');
                        var extent = markerSource.getExtent();
                        map.getView().fit(extent, { padding: [100, 100, 100, 100] });


                        select_interaction = new ol.interaction.Select();
                        select_interaction.setHitTolerance(25);

                        select_interaction.getFeatures().on("add", function (e) { 

                            var feature = e.element; //the feature selected
                            console.log(feature);
                            var html = '<h3>'+feature.get('name')+' <small>Projects</small></h3>';

                            $.each(feature.get('projects'), function(i, proj){
                                var project = self.project_data[proj];
                                html += '<h5>'+project.name+' - <small>'+project.fund+'</small></h5>';
                                // html += '<ul>';
                                // $.each(fund.projects, function(i, project){
                                //     if (project.link.length){
                                //         html += '<li><a href="'+project.link+'" target="_blank">'+project.name+'</a></li>'; 
                                //     } else {
                                //         html += '<li>'+project.name+'</li>';    
                                //     }

                                // });
                                // html += '</ul>';
                            })

                          popupContent.innerHTML = html;

                          overlay.setPosition(feature.getGeometry().getCoordinates());
                        });

                        map.addInteraction(select_interaction);
                    }
                    // console.log(markerSource.getExtent());




                }

            }

        })



    }

});
